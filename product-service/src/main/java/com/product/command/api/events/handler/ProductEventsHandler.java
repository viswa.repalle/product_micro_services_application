package com.product.command.api.events.handler;

import com.product.command.api.events.ProductCreatedEvent;
import com.product.exception.ProductExistsException;
import com.product.model.Product;
import com.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductEventsHandler {
    @Autowired
    private final ProductRepository productRepository;
    @EventHandler
    public Product on(ProductCreatedEvent productEvent){
        Product product = new Product();
        BeanUtils.copyProperties(productEvent,product);
        if(productRepository.findAll().stream().anyMatch(productFromDatabase -> productFromDatabase.equals(product)))
            throw new ProductExistsException();
        else
            return productRepository.save(product);
    }
}
