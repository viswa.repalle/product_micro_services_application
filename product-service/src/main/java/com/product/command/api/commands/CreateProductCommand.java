package com.product.command.api.commands;

import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
public class CreateProductCommand {
    private Integer productId;
    @TargetAggregateIdentifier
    private String name;
    private float price;
    private int quantity;
}
