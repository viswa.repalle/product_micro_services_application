package com.product.command.api.controller;

import com.product.command.api.commands.CreateProductCommand;
import com.product.dto.ProductDto;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductCommandController {
    private final CommandGateway commandGateway;
    @PostMapping("/add")
    public ResponseEntity<String> addProduct(@RequestBody ProductDto product) {
        CreateProductCommand createProductCommand = CreateProductCommand.builder()
                .name(product.getName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
        String result=commandGateway.sendAndWait(createProductCommand);
        return ResponseEntity.ok(result);
    }

}
