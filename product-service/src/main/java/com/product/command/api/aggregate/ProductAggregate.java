package com.product.command.api.aggregate;

import com.product.command.api.commands.CreateProductCommand;
import com.product.command.api.events.ProductCreatedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
public class ProductAggregate {
    private Integer productId;
    @AggregateIdentifier
    private String name;
    private float price;
    private int quantity;
    @CommandHandler
    public ProductAggregate(CreateProductCommand createProductCommand){
        ProductCreatedEvent productCreatedEvent = new ProductCreatedEvent();
        BeanUtils.copyProperties(createProductCommand,productCreatedEvent);
        AggregateLifecycle.apply(productCreatedEvent);
    }
    @EventSourcingHandler
    public void on(ProductCreatedEvent productCreatedEvent){
      this.name = productCreatedEvent.getName();
      this.price = productCreatedEvent.getPrice();
      this.productId = productCreatedEvent.getProductId();
      this.quantity = productCreatedEvent.getQuantity();
    }
}
