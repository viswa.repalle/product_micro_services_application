package com.product.query.api.controller;

import com.product.model.Product;
import com.product.query.api.queries.GetAllProductsQuery;
import lombok.RequiredArgsConstructor;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductQueryController {

    private final QueryGateway queryGateway;
    @GetMapping("/list")
    public ResponseEntity<List<Product>> findAllProducts(){
        List<Product> products =
                queryGateway.query(new GetAllProductsQuery(), ResponseTypes.multipleInstancesOf(Product.class))
                        .join();
        return ResponseEntity.ok(products);
    }
}
