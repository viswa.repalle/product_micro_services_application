package com.product.query.api.projection;

import com.product.model.Product;
import com.product.query.api.queries.GetAllProductsQuery;
import com.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RequiredArgsConstructor
public class ProductProjection {
    @Autowired
    private final ProductRepository productRepository;
    @QueryHandler
    public List<Product> handle(GetAllProductsQuery getAllProductsQuery){
        return productRepository.findAll();
    }
}
