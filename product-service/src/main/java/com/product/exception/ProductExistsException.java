package com.product.exception;

public class ProductExistsException extends RuntimeException{
    public ProductExistsException(){
        super("Product Already Exists");
    }
}
