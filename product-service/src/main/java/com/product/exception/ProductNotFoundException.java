package com.product.exception;

public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException(Integer id){
        super("Product Not Found With Id "+id);
    }
}
