package com.product.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {
    private Integer productId;
    private String name;
    private float price;
    private int quantity;
}
