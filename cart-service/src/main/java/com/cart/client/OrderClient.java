package com.cart.client;

import com.cart.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="ORDER-CLIENT",url="http://localhost:9191/orders")
public interface OrderClient {
    @PostMapping("/add")
    Order addOrder(Order order);
}
