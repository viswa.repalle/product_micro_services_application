package com.cart.service;

import com.cart.client.OrderClient;
import com.cart.exception.CannotOrderException;
import com.cart.model.Cart;
import com.cart.model.Order;
import com.cart.model.Product;
import com.cart.repository.CartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static com.cart.model.CartStatus.NOT_ORDERED;
import static com.cart.model.OrderStatus.PENDING;

@Service
@RequiredArgsConstructor
public class CartService {
    @Autowired
    private final OrderClient orderClient;
    @Autowired
    private final CartRepository cartRepository;

    private Order addOrder(List<Product> products){
        float price = 0F;
        Iterator<Float> priceNumbers=products.stream().map(Product::getPrice).iterator();
        Iterator<Integer> quantityNumbers=products.stream().map(Product::getQuantity).iterator();
        while(priceNumbers.hasNext()){
            price+=(priceNumbers.next() * quantityNumbers.next());
        }
        Cart cart = findCartByNotOrdered();
        cartRepository.deleteById(cart.getId());
        cartRepository.save(cart);
        Order order = Order.builder().products(products).price(price).status(PENDING).build();
        return orderClient.addOrder(order);
    }
    public Cart addToCart(Product product){
        Cart cart=findCartByNotOrdered();
        if(cart==null) {
            return Cart.builder().products(List.of(product)).cartStatus(NOT_ORDERED).build();
        }
        else{
            List<Product> products = cart.getProducts();
            products.add(product);
            cart.setProducts(products);
            return cart;
        }
    }
    private Cart findCartByNotOrdered(){
        Optional<Cart> cartOptional = cartRepository.findAll().stream()
                    .filter(cart -> cart.getCartStatus().getStatus().equals("NOT_ORDERED"))
                    .findFirst();
            return cartOptional.orElseThrow(CannotOrderException::new);
    }

    public Order buy(){
        return addOrder(findCartByNotOrdered().getProducts());
    }
}
