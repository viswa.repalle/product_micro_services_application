package com.cart.model;

import lombok.Getter;

@Getter
public enum OrderStatus {
    PENDING("PENDING",1),CANCELLED("CANCELLED",2), COMPLETED("COMPLETED",3);
    OrderStatus(String status,Integer id){
        this.status=status;
        this.id=id;
    }
    private final String status;
    private final Integer id;
}
