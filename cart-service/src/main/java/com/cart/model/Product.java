package com.cart.model;

import lombok.Data;

@Data
public class Product {
    private Integer id;
    private String name;
    private float price;
    private int quantity;
}
