package com.cart.model;

import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
public enum CartStatus {
    NOT_ORDERED("NOT_ORDERED",1),ORDERED("ORDERED",2);
    CartStatus(String status,Integer id){
        this.status = status;
        this.id=id;
    }
    private final String status;
    @Id
    private final Integer id;
}
