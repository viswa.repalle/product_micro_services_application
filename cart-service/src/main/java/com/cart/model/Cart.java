package com.cart.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@Builder
public class Cart {
    @Id
    private Integer id;
    private List<Product> products;
    private CartStatus cartStatus;
}
