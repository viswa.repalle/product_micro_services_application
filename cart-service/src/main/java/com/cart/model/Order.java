package com.cart.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Order {
    private Integer id;
    private OrderStatus status;
    private float price;
    private List<Product> products;
}
