package com.cart.exception;

public class CannotOrderException extends RuntimeException{
    public CannotOrderException() {
        super("Cannot Buy Because There Is Not A Single Product");
    }
}
