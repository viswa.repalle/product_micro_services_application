package com.cart.controller;

import com.cart.model.Cart;
import com.cart.model.Order;
import com.cart.model.Product;
import com.cart.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private final CartService cartService;

    @PostMapping("/addToCart")
    public ResponseEntity<Cart> addProductToCart(@RequestBody Product product){
        return ResponseEntity.ok(cartService.addToCart(product));
    }
    @PostMapping("/buy")
    public ResponseEntity<Order> buy(){
        return ResponseEntity.ok(cartService.buy());
    }

}
