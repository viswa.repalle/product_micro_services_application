package com.payment.service;

import com.payment.dto.CardDetailsDto;
import com.payment.dto.PaymentDto;
import com.payment.model.CardDetails;
import com.payment.model.Payment;
import com.payment.model.PaymentStatus;
import com.payment.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentService {
    @Autowired
    private final PaymentRepository paymentRepository;
    public Payment addPayment(PaymentDto paymentDto){
        if(resolveCardDetails(paymentDto.getCardDetails())){
            CardDetailsDto dto = paymentDto.getCardDetails();
            CardDetails cardDetails = CardDetails.builder()
                    .cardNumber(dto.getCardNumber())
                    .cvv(dto.getCvv())
                    .expiryMonth(dto.getExpiryMonth())
                    .expiryYear(dto.getExpiryYear())
                    .holderName(dto.getHolderName())
                    .cardType(dto.getCardType())
                    .build();
            Payment payment = Payment.builder()
                    .paymentStatus(PaymentStatus.PENDING)
                    .cardDetails(cardDetails)
                    .price(paymentDto.getPrice())
                    .build();
            return paymentRepository.save(payment);
        }
        return null;
    }
    private boolean resolveCardDetails(CardDetailsDto cardDetailsDto){
        return cardDetailsDto.getCardNumber().length() == 10
                && cardDetailsDto.getCvv().toString().length() == 3
                && cardDetailsDto.getExpiryMonth() <= 12
                && cardDetailsDto.getExpiryYear() >= java.time.LocalDate.now().getYear() + 6
                && cardDetailsDto.getHolderName().length()>2
                && cardDetailsDto.getCardNumber().replaceAll("[0-9]","").length()==0;
    }
}
