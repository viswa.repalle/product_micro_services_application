package com.payment.model;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;
@Getter
@Entity
public enum CardType {
    DEBIT("DEBIT_CARD",1),CREDIT("CREDIT_CARD",2);
    CardType(String type,Integer id){
        this.type=type;
        this.id=id;
    }
    @Id
    private final Integer id;
    private final String type;
}
