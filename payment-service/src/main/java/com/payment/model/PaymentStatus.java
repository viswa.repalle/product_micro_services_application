package com.payment.model;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
public enum PaymentStatus {
    PENDING("PENDING",1),COMPLETED("COMPLETED",2);

    PaymentStatus(String status,Integer id){
        this.status=status;
        this.id=id;
    }
    private final String status;
    @Id
    private final Integer id;
}
