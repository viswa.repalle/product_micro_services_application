package com.payment.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Builder
public class CardDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String cardNumber;
    private Integer expiryMonth;
    private Integer expiryYear;
    private String holderName;
    private Integer cvv;
    @OneToOne
    private CardType cardType;
}