package com.payment.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Data
@Table
@Builder
public class Payment {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer paymentId;
    @OneToOne
    private PaymentStatus paymentStatus;
    @OneToOne(cascade = ALL)
    private CardDetails cardDetails;
    private float price;
}
