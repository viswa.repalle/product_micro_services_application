package com.payment.dto;

import com.payment.model.CardType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CardDetailsDto {
    private Integer id;
    private String cardNumber;
    private Integer expiryMonth;
    private Integer expiryYear;
    private String holderName;
    private Integer cvv;
    private CardType cardType;
}
