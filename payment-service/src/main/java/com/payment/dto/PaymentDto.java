package com.payment.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentDto {
    private Integer paymentId;
    private CardDetailsDto cardDetails;
    private float price;
}
