package com.payment.controller;

import com.payment.dto.PaymentDto;
import com.payment.model.Payment;
import com.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/payments")
public class PaymentController {
    @Autowired
    private final PaymentService paymentService;
    @PostMapping("/add")
    public ResponseEntity<Payment> addPayment(@RequestBody PaymentDto payment){
        return ResponseEntity.ok(paymentService.addPayment(payment));
    }
}
