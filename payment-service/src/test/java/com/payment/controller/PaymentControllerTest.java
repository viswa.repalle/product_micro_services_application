package com.payment.controller;

import com.payment.dto.CardDetailsDto;
import com.payment.dto.PaymentDto;
import com.payment.model.CardType;
import com.payment.model.Payment;
import com.payment.service.PaymentService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class PaymentControllerTest {
    @Autowired
    private PaymentService paymentService;

    @BeforeEach
    void setUp () {
    }

    @AfterEach
    void tearDown () {
    }

    @Test
    void addPayment () {
        CardDetailsDto cardDetails = CardDetailsDto.builder()
                .cardType(CardType.DEBIT)
                .holderName("Akhil Repalle")
                .expiryYear(2028)
                .expiryMonth(10)
                .cvv(123)
                .cardNumber("1234567890")
                              .build();
        Payment payment=paymentService.addPayment(PaymentDto.builder()
                        .price(1000)
                        .cardDetails(cardDetails)
                             .build());
        assertThat(payment).isNotNull();

    }
}