package com.order.model;

import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@Getter
public enum OrderStatus {
    PENDING("PENDING",1),CANCELLED("CANCELLED",2), COMPLETED("COMPLETED",3);
    OrderStatus(String status,Integer id){
        this.status=status;
        this.id=id;
    }
    private final String status;
    @Id
    private final Integer id;
}
