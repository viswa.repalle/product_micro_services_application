package com.order.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Data
@Document
public class Order {
    @Id
    private Integer id;
    private OrderStatus status;
    private float price;
    private List<Product> products;
}