package com.order.service;

import com.order.model.Order;
import com.order.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {
    @Autowired
    private final OrderRepository orderRepository;
    public Order addOrder(Order order){
        order.setStatus(com.order.model.OrderStatus.PENDING);
        return orderRepository.save(order);
    }
}
