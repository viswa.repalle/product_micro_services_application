package com.customer.controller;

import com.customer.model.Cart;
import com.customer.model.Customer;
import com.customer.model.Product;
import com.customer.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private final CustomerService customerService;
    @PostMapping("/add")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(customerService.addCustomer(customer));
    }
    @GetMapping("/list")
    public ResponseEntity<List<Customer>> findAllCustomers(){
        return ResponseEntity.ok(customerService.findAllCustomers());
    }
    @PostMapping("/addProductToCart")
    public ResponseEntity<Cart> addProductToCart(@RequestBody Product product){
        return ResponseEntity.ok(customerService.addProductToCart(product));
    }
}
