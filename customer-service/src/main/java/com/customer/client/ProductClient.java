package com.customer.client;

import com.customer.model.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name="PRODUCT-CLIENT",url="http://localhost:9191/products/")
public interface ProductClient {
   @GetMapping("list")
   List<Product> findAllProducts();
}
