package com.customer.client;

import com.customer.model.Cart;
import com.customer.model.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(url = "http://localhost:9191/cart/",
             name = "CART-CLIENT")
public interface CartClient {
   @PostMapping("addToCart")
   Cart addProductToCart(Product product);
}
