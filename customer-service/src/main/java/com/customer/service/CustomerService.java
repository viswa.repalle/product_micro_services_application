package com.customer.service;

import com.customer.client.CartClient;
import com.customer.client.ProductClient;
import com.customer.exception.ProductNotFoundException;
import com.customer.exception.ProductsQuantityIsGreaterException;
import com.customer.model.Cart;
import com.customer.model.Customer;
import com.customer.model.Product;
import com.customer.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService {
    @Autowired
    private final CustomerRepository customerRepository;
    @Autowired
    private final ProductClient productClient;
    @Autowired
    private final CartClient cartClient;
    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }
    public List<Customer> findAllCustomers(){
        return customerRepository.findAll();
    }
    public Cart addProductToCart(Product product){
        boolean isProductInTheDatabase=productClient.findAllProducts()
                        .stream()
                        .anyMatch(productFromDatabase -> productFromDatabase.equalsWithoutQuantity(product));
        if(isProductInTheDatabase) {
            Product fromDatabase=productClient.findAllProducts()
                    .stream()
                    .filter(productFromDatabase -> productFromDatabase.equalsWithoutQuantity(product))
                    .collect(Collectors.toList()).get(0);
            if (fromDatabase.getQuantity()>=product.getQuantity()) {
                return cartClient.addProductToCart(product);
            }
            else throw new ProductsQuantityIsGreaterException(fromDatabase.getQuantity(),product.getQuantity());

        }
        else throw new ProductNotFoundException();
    }
}
