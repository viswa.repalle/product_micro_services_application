package com.customer.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Product {
    private Integer productId;
    private String name;

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Float.compare(product.price, price) == 0 && quantity == product.quantity && name.equals(product.name);
    }

    public boolean equalsWithoutQuantity(Product o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return Float.compare(o.price, price) == 0 && name.equals(o.name);
    }


    @Override
    public int hashCode () {
        return Objects.hash(name, price, quantity);
    }

    private float price;
    private int quantity;
}
