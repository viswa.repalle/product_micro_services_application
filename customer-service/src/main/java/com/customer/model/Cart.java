package com.customer.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class Cart {
    private Integer id;
    private List<Product> products;
    private CartStatus status;
}
