package com.customer.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Data
public class Customer {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer customerId;
    private String username;
    private String email;
    private String password;
    private String phoneNumber;
}
