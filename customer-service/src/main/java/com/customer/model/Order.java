package com.customer.model;

import lombok.Data;

import java.util.List;

@Data
public class Order {
    private Integer id;
    private OrderStatus status;
    private float price;
    private List<Product> products;
}
