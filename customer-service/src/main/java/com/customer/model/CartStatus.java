package com.customer.model;

import lombok.Getter;

@Getter
public enum CartStatus {
    NOT_ORDERED("NOT_ORDERED",1),ORDERED("ORDERED",2);
    CartStatus(String status,Integer id){
        this.status = status;
        this.id=id;
    }
    private final String status;
    private final Integer id;
}
