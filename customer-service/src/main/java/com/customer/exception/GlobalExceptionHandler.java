package com.customer.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = ProductNotFoundException.class)
    public ResponseEntity<String> productNotFound(ProductNotFoundException ex){
        return new ResponseEntity<>(ex.getMessage(),NOT_FOUND);
    }
    @ExceptionHandler(value = ProductsQuantityIsGreaterException.class)
    public ResponseEntity<String> productQuantityIsGreater(ProductsQuantityIsGreaterException ex){
        return new ResponseEntity<>(ex.getMessage(),NOT_FOUND);
    }

}
