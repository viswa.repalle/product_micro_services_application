package com.customer.exception;

public class ProductNotFoundException extends RuntimeException{
    public ProductNotFoundException() {
        super("Product Cannot Added To The Cart Because We Cannot Give You The Product You Asked For");
    }

}
