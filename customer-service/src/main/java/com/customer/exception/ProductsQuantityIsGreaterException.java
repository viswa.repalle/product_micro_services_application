package com.customer.exception;

public class ProductsQuantityIsGreaterException extends RuntimeException {
    private int availableQuantity;
    private int greaterQuantity;
    public ProductsQuantityIsGreaterException(int availableQuantity,int greaterQuantity){
        super("We Cannot Give You The Quantity of "+greaterQuantity +" Because Of The Current Quantity Is "+greaterQuantity);
        this.availableQuantity=availableQuantity;
        this.greaterQuantity=greaterQuantity;
    }
}
